import json
import logging
import os
import random

import matplotlib.pyplot as plt
import numpy as np
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.metrics import (accuracy_score, confusion_matrix, f1_score,
                             precision_score, silhouette_score)
from sklearn.model_selection import ParameterGrid, train_test_split

from utils.generator import DataGenerator

random.seed(42)
np.random.seed(42)
TOLERANCE = 5
BATCH_SIZE = 100
logger = logging.getLogger("tipper")
logger.setLevel(logging.DEBUG)


def main():
    with open("utils/config.json") as config_file:
        data = json.load(config_file)

    partition = {"train": [], "validation": [], "test": []}

    for file in os.listdir(data["data_chunked_folder"] + "train"):
        if file.startswith("id-tr"):
            partition["train"].append(str(file.split(".")[0]))
    for file in os.listdir(data["data_chunked_folder"] + "valid"):
        if file.startswith("id-val"):
            partition["validation"].append(str(file.split(".")[0]))
    for file in os.listdir(data["data_chunked_folder"] + "test"):
        if file.startswith("id-te"):
            partition["test"].append(str(file.split(".")[0]))

    train_generator = DataGenerator(
        partition["train"], logger, TOLERANCE, data, "train"
    )
    valid_generator = DataGenerator(
        partition["validation"], logger, TOLERANCE, data, "valid"
    )

    # refactor data for ML
    i = 0
    SAMPLES = 150
    X = []
    Y = []
    for el in train_generator:
        X.append(el[0][1])
        Y.append(el[1])
        i = i + 1
        if i > SAMPLES:
            break

    X_finn = np.vstack(np.vstack(np.array(X)))

    Y_fin = np.hstack(np.array(Y))

    Y_finn = np.repeat(Y_fin, 10)

    pca = PCA(n_components=1)
    X_finn_reduced = pca.fit_transform(X_finn)

    MID = int(X_finn_reduced.shape[0] * 0.6)
    # X_train, X_test, y_tr, y_te = train_test_split(X_finn_reduced, Y_finn)
    X_train = X_finn_reduced[MID:, :]
    y_tr = Y_finn[MID:]
    X_test = X_finn_reduced[:MID, :]
    y_te = Y_finn[:MID]
    print("Train size: ", X_train.shape)
    print("Test size: ", X_test.shape)
    X_good_tr = X_train[np.logical_not(y_tr)]

    kmeans = KMeans(n_clusters=2)
    kmeans.fit(X_good_tr)
    cluster_centers = kmeans.cluster_centers_
    label = kmeans.predict(X_test)

    distances = [
        np.linalg.norm(x - cluster_centers[cluster])
        for x, cluster in zip(X_test, label)
    ]

    for percentile_threshold in range(80, 100):
        threshold_distance = np.percentile(distances, percentile_threshold)

        anomalies = [distance > threshold_distance for distance in distances]
        anomalies = np.asarray(anomalies, dtype=np.float32)

        matrix = confusion_matrix(y_te, anomalies)

        tn, fp, fn, tp = matrix.ravel()

        spec = tn / (tn + fp)

        sens = tp / (tp + fn)
        print(
            percentile_threshold,
            accuracy_score(y_te, anomalies),
            precision_score(y_te, anomalies),
            f1_score(y_te, anomalies),
            spec,
            sens,
        )

    print("Done")


def choose_k(X):
    # # candidate values for our number of cluster
    parameters = [2, 3, 4, 5, 10, 15, 20, 25, 30, 35, 40]
    # instantiating ParameterGrid, pass number of clusters as input
    parameter_grid = ParameterGrid({"n_clusters": parameters})
    best_score = -1
    kmeans_model = KMeans()  # instantiating KMeans model
    silhouette_scores = []
    # evaluation based on silhouette_score
    for p in parameter_grid:
        kmeans_model.set_params(**p)  # set current hyper parameter
        kmeans_model.fit(
            X
        )  # fit model on wine dataset, this will find clusters based on parameter p
        ss = silhouette_score(X, kmeans_model.labels_)  # calculate silhouette_score
        silhouette_scores += [ss]  # store all the scores
        print("Parameter:", p, "Score", ss)
        # check p which has the best score
        if ss > best_score:
            best_score = ss
            best_grid = p
    # plotting silhouette score
    plt.bar(
        range(len(silhouette_scores)),
        list(silhouette_scores),
        align="center",
        color="#722f59",
        width=0.5,
    )
    plt.xticks(range(len(silhouette_scores)), list(parameters))
    plt.title("Silhouette Score", fontweight="bold")
    plt.xlabel("Number of Clusters")
    plt.show()


if __name__ == "__main__":
    main()
