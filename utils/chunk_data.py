import json

import h5py
import numpy as np

DATA_TYPE_TO_SHORT = {"train": "tr", "valid": "val", "test": "te"}
TOLERANCE = 5


class Chunk:
    def __init__(self, chunk_size=100, save=False):
        self.chunk_size = chunk_size
        self.save = save
        self.data_vac = []
        self.vac_times = []
        self.vac_data_len = None
        with open("config.json") as config_file:
            self.data = json.load(config_file)

    def chunk_all(self):
        self.chunk_data("train")
        self.clear_vac()
        self.chunk_data("valid")
        self.clear_vac()
        self.chunk_data("test")

    def clear_vac(self):
        self.data_vac = None
        self.data_vac = []
        self.vac_times = []
        self.vac_data_len = None

    def chunk_data(self, data_type):
        counter = 1
        for f in self.data[f"{data_type}_data_vac"]:
            hf = h5py.File(f, "r")
            x_vac = hf.get("data").value
            self.data_vac.extend(x_vac[:, 1:])
            self.vac_times.extend(x_vac[:, 0])
        self.data_vac = np.array(self.data_vac)
        self.vac_times = np.array(self.vac_times)
        self.vac_data_len = self.data_vac.shape[0]
        for dat in self.data[f"{data_type}_data_pinhole"]:
            xdata = None
            ydata = None
            zdata = None
            hf = h5py.File(dat, "r")
            xdata = hf.get("image_data").value
            ydata = hf.get("labels").value
            zdata = hf.get("scalar_data").value
            chunks = xdata.shape[0] // self.chunk_size
            if self.save:
                print(f"Saving {dat} {chunks} chunks")
                for i in range(chunks):
                    x = xdata[i * self.chunk_size : (i + 1) * self.chunk_size, :, :]
                    y = ydata[i * self.chunk_size : (i + 1) * self.chunk_size]
                    z = zdata[i * self.chunk_size : (i + 1) * self.chunk_size, :]
                    vac = self._generateVac(z[:, 0])
                    with open(
                        f"{self.data['data_chunked_folder']}/{data_type}/id-{DATA_TYPE_TO_SHORT[data_type]}-{counter}.npy",
                        "wb",
                    ) as f:
                        np.save(f, x)
                        np.save(f, y)
                        np.save(f, z)
                        np.save(f, vac)
                    counter += 1

    def _generateVac(self, pin_times):
        X = []
        for t in pin_times:
            vac_mid_ind = np.argmin(abs(self.vac_times - t))
            if (vac_mid_ind - 2 * TOLERANCE) < 0:
                # beginning
                if (vac_mid_ind - TOLERANCE) < 0:
                    X.append(self.data_vac[0 : 2 * TOLERANCE])
                else:
                    X.append(
                        self.data_vac[vac_mid_ind - TOLERANCE : vac_mid_ind + TOLERANCE]
                    )
            else:
                X.append(self.data_vac[vac_mid_ind - 2 * TOLERANCE : vac_mid_ind])
        return np.array(X)


if __name__ == "__main__":
    obj = Chunk(save=True)
    obj.chunk_all()
