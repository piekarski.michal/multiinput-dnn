import numpy as np
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.applications.inception_v3 import preprocess_input
from tensorflow.keras.utils import Sequence, to_categorical


class DataGenerator(Sequence):
    def __init__(
        self,
        list_IDs,
        logger,
        tolerance,
        config,
        type,
        batch_size=100,
        file_batch=100,
        to_fit=True,
        shuffle=True,
    ):
        self.batch_size = batch_size
        self.file_batch = file_batch
        self.to_fit = to_fit
        self.shuffle = shuffle
        self.list_IDs = list_IDs
        self.logger = logger
        self.config = config
        self.type = type
        self.indexes = np.arange(len(self.list_IDs))

    def __len__(self):
        """Denotes the number of batches per epoch
        :return: number of batches per epoch
        """
        return int((np.floor(len(self.list_IDs) * self.file_batch) / self.batch_size))

    def __getitem__(self, index):
        """Generate one batch of data
        :param index: index of the batch
        :return: X and y when fitting. X only when predicting
        """
        # Generate indexes of the batch
        num_of_files = self.batch_size // self.file_batch
        indexes = self.indexes[index * num_of_files : (index + 1) * num_of_files]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        if self.to_fit:
            X, y = self.__data_generation(list_IDs_temp)
            return X, y
        else:
            X = self.__data_generation(list_IDs_temp)
            return X

    def on_epoch_end(self):
        "Updates indexes after each epoch"
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        "Generates data containing batch_size samples"  # X : (n_samples, *dim, n_channels)
        for i, ID in enumerate(list_IDs_temp):
            with open(
                f"{self.config['data_chunked_folder']}{self.type}/{ID}.npy", "rb"
            ) as f:
                _x = np.load(f)
                _y = np.load(f)
                _z = np.load(f)
                X_vac = np.load(f)
            X_pin = np.array(
                [preprocess_input(np.stack((el, el, el), axis=-1)) for el in _x]
            )
            mmsk = MinMaxScaler()
            vac_scaled = np.array([mmsk.fit_transform(el) for el in X_vac])
        if self.to_fit:
            return [X_pin, vac_scaled], to_categorical(_y, num_classes=2, dtype=bool)
        else:
            return [X_pin, vac_scaled]
