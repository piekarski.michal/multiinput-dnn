import tensorflow as tf
from keras.layers import (Conv1D, Dense, Dropout, Flatten,
                          GlobalAveragePooling2D, Input, MaxPooling1D)
from keras.models import Model
from tensorflow.keras.applications import InceptionV3

tf.random.set_seed(42)
TOLERANCE = 5
IMAGE_SIZE_pin = (134, 390)
NUM_CLASSES = 2
input_shape_pin = (IMAGE_SIZE_pin[0], IMAGE_SIZE_pin[1], 3)
input_shape_vac = (TOLERANCE * 2, 64)


class DNNModel:
    def __init__(
        self,
    ):
        self.model = None

    def create_model(self):
        inc_model = InceptionV3(
            input_shape=input_shape_pin, include_top=False, weights="imagenet"
        )
        inc_model.trainable = False
        layers = inc_model.layers
        layers[-1].trainable = True
        layers[-2].trainable = True
        layers[-3].trainable = True

        pin_inputs = Input(shape=input_shape_pin)
        img = inc_model(pin_inputs, training=False)
        img = GlobalAveragePooling2D()(img)
        img = Dense(500, activation="leaky_relu", name="inc_out")(img)
        img_output = Dropout(0.7)(img)

        cnn_input = Input(shape=input_shape_vac)

        conv = Conv1D(
            filters=64, kernel_size=5, padding="same", activation="leaky_relu"
        )(cnn_input)
        conv = Conv1D(
            filters=64, kernel_size=5, padding="same", activation="leaky_relu"
        )(conv)
        conv = Dropout(0.7)(conv)
        conv = MaxPooling1D(pool_size=2)(conv)
        conv = Flatten()(conv)
        cnn_output = Dense(100, activation="leaky_relu", name="cnn_out")(conv)

        x = tf.keras.layers.concatenate([img_output, cnn_output], name="concat_inv_vgg")

        predictions = Dense(NUM_CLASSES, activation="softmax")(x)
        self.model = Model(inputs=[pin_inputs, cnn_input], outputs=predictions)
