import logging
import time

import h5py
import numpy as np
from PyTangoArchiving import Reader

logging.getLogger().setLevel(logging.DEBUG)


class HDF5Reader(object):
    def __init__(self, db, conf, attr):
        self._db = db
        self._c = conf
        self._attr = attr
        self.reader = None
        self._connection = False
        self.values = None
        self.connect()

    def connect(self):
        try:
            self.reader = Reader(db=self._db, config=self._c)
        except Exception as e:
            logging.error("Exception {}".format(e))
            self._connection = False
        else:
            self._connection = True

    def readAttribute(self, start, stop):
        if self._connection:
            try:
                self.values = None
                logging.debug("Started reading attrs...")
                self.values = self.reader.get_attributes_values(
                    self._attr,
                    start,
                    stop,
                    correlate=True,
                )
                logging.debug("Finished reading {} attrs!".format(len(self.values)))
            except Exception as e:
                self._connection = False
                logging.error("Exception {}".format(e))

    def saveData(self, day):
        if self.values is None or self._connection is False:
            logging.debug("Nothing to save! Aborting...")
        else:
            logging.debug("Processing {} data...".format(len(self.values)))
            datastack = None
            labels = ["timestamp"]
            tmp = []
            for tup in self.values[self.values.keys()[0]]:
                tmp.append(tup[0])
            datastack = np.array(tmp)
            for key in self.values.keys():
                dat = []
                for tup in self.values[key]:
                    if tup[1] is None:
                        val = 0
                    else:
                        val = tup[1]
                    dat.append(val)
                labels.append(key)
                datastack = np.vstack((datastack, dat))
            datastack = datastack.T
            logging.debug("Saving {} data...".format(len(datastack)))
            date = day
            file = h5py.File("data/{}.h5".format(date), "w")
            file.create_dataset("data", np.shape(datastack), data=datastack)
            file.create_dataset("meta", np.shape(labels), data=labels)
            file.close()
            logging.debug("Data saved to data/{}.h5!".format(date))

    def readData(self, start, stop):
        if not self._connection:
            time.sleep(30)
            self.connect()
        self.readAttribute(start + " 00:00:00", stop + " 23:59:59")


def main():
    attrs = []
    rd = HDF5Reader(
        DB,
        CONFIG,
        attrs,
    )
    start_date = ""
    stop_date = ""

    rd.readData(start_date, stop_date)
    rd.saveData("scalar_data")


if __name__ == "__main__":
    main()
