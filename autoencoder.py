import json
import logging
import os
import random

import keras
import matplotlib.pyplot as plt
import numpy as np
from keras import layers
from keras.layers import LSTM, Dense, Input, RepeatVector, TimeDistributed
from keras.models import Model, Sequential
from sklearn.decomposition import PCA
from sklearn.metrics import (accuracy_score, confusion_matrix, f1_score,
                             precision_score)
from sklearn.model_selection import train_test_split

from utils.generator import DataGenerator

random.seed(42)
np.random.seed(42)
TOLERANCE = 5
BATCH_SIZE = 100
logger = logging.getLogger("tipper")
logger.setLevel(logging.DEBUG)


def main():
    with open("utils/config.json") as config_file:
        data = json.load(config_file)

    partition = {"train": [], "validation": [], "test": []}

    for file in os.listdir(data["data_chunked_folder"] + "train"):
        if file.startswith("id-tr"):
            partition["train"].append(str(file.split(".")[0]))
    for file in os.listdir(data["data_chunked_folder"] + "valid"):
        if file.startswith("id-val"):
            partition["validation"].append(str(file.split(".")[0]))
    for file in os.listdir(data["data_chunked_folder"] + "test"):
        if file.startswith("id-te"):
            partition["test"].append(str(file.split(".")[0]))

    train_generator = DataGenerator(
        partition["train"], logger, TOLERANCE, data, "train"
    )

    # refactor data for ML
    i = 0
    SAMPLES = 150
    X = []
    Y = []
    for el in train_generator:
        X.append(el[0][1])
        Y.append(el[1])
        i = i + 1
        if i > SAMPLES:
            break

    X_finn = np.vstack(np.vstack(np.array(X)))

    Y_fin = np.hstack(np.array(Y))

    Y_finn = np.repeat(Y_fin, 10)

    plt.plot(X_finn)
    plt.title("Raw data scaled")
    plt.show()

    # reduce PCA
    pca = PCA(n_components=1)
    X_finn_reduced = pca.fit_transform(X_finn)
    plt.plot(X_finn_reduced)
    plt.title("PCA reduced")
    plt.show()

    MID = int(X_finn_reduced.shape[0]*0.6)
    # X_train, X_test, y_tr, y_te = train_test_split(X_finn_reduced, Y_finn)
    X_train = X_finn_reduced[MID:, :]
    y_tr = Y_finn[MID:]
    X_test = X_finn_reduced[:MID, :]
    y_te = Y_finn[:MID]
    print("Train size: ", X_train.shape)
    print("Test size: ", X_test.shape)
    # only good samples
    X_good_tr = X_train[np.logical_not(y_tr)]
    print("Not anomaly size: ", X_good_tr.shape)
    plt.plot(X_good_tr)
    plt.title("Good only")
    plt.show()
    # time windows
    TIME_STEPS = 100
    output = []
    for i in range(len(X_good_tr) - TIME_STEPS + 1):
        output.append(X_good_tr[i : (i + TIME_STEPS)])
    output = np.stack(output)

    model = keras.Sequential(
        [
            layers.Input(shape=(output.shape[1], output.shape[2])),
            layers.Conv1D(
                filters=32,
                kernel_size=7,
                padding="same",
                strides=2,
                activation="relu",
            ),
            layers.Dropout(rate=0.3),
            layers.Conv1D(
                filters=16,
                kernel_size=7,
                padding="same",
                strides=2,
                activation="relu",
            ),
            layers.Conv1DTranspose(
                filters=16,
                kernel_size=7,
                padding="same",
                strides=2,
                activation="relu",
            ),
            layers.Dropout(rate=0.3),
            layers.Conv1DTranspose(
                filters=32,
                kernel_size=7,
                padding="same",
                strides=2,
                activation="relu",
            ),
            layers.Conv1DTranspose(filters=1, kernel_size=7, padding="same"),
        ]
    )
    model.compile(optimizer="adam", loss="mse")
    model.summary()

    history = model.fit(
        output,
        output,
        epochs=50,
        batch_size=128,
        validation_split=0.1,
        callbacks=[
            keras.callbacks.EarlyStopping(monitor="val_loss", patience=5, mode="min")
        ],
    )

    x_train_pred = model.predict(output)
    train_mae_loss = np.mean(np.abs(x_train_pred - output), axis=1)
    plt.plot(train_mae_loss)
    plt.title("Train loss")
    plt.show()
    # Get reconstruction loss threshold.
    threshold = np.max(train_mae_loss)*0.9
    print("Reconstruction error threshold: ", threshold)

    testt = []
    testty = []
    for i in range(len(X_test) - TIME_STEPS + 1):
        testt.append(X_test[i : (i + TIME_STEPS)])
        testty.append(y_te[i : (i + TIME_STEPS)])
    x_test = np.stack(testt)
    testty = [np.bincount(el).argmax() for el in testty]
    print("Anomalies:", sum(testty)/len(testty))

    x_test_pred = model.predict(x_test)
    test_mae_loss = np.mean(np.abs(x_test_pred - x_test), axis=1)
    test_mae_loss = test_mae_loss.reshape((-1))
    plt.plot(test_mae_loss)
    plt.show()
    anomalies = test_mae_loss > threshold

    print(
        "Model 1:",
        accuracy_score(testty, anomalies),
        precision_score(testty, anomalies),
        f1_score(testty, anomalies),
    )
    matrix = confusion_matrix(testty, anomalies)

    tn, fp, fn, tp = matrix.ravel()

    spec = tn / (tn + fp)

    sens = tp / (tp + fn)

    print("Model1", spec, sens)

    # model2
    ext_lstm_units = 64
    int_lstm_units = 32

    input_layer = Input(shape=(output.shape[1], output.shape[2]))
    encoder = LSTM(ext_lstm_units, return_sequences=True)(input_layer)
    encoder_1 = LSTM(int_lstm_units)(encoder)
    repeat = RepeatVector(output.shape[1])(encoder_1)
    decoder = LSTM(int_lstm_units, return_sequences=True)(repeat)
    decoder_1 = LSTM(ext_lstm_units, return_sequences=True)(decoder)
    output_layer = TimeDistributed(Dense(output.shape[2]))(decoder_1)

    model2 = Model(inputs=input_layer, outputs=output_layer)
    model2.compile(optimizer="adam", loss="mse")

    history2 = model2.fit(
        output,
        output,
        epochs=50,
        batch_size=128,
        validation_split=0.1,
        callbacks=[
            keras.callbacks.EarlyStopping(monitor="val_loss", patience=5, mode="min")
        ],
    )

    x_train_pred2 = model2.predict(output)
    train_mae_loss2 = np.mean(np.abs(x_train_pred2 - output), axis=1)

    # Get reconstruction loss threshold.
    threshold2 = np.max(train_mae_loss2)*0.9
    print("Reconstruction error threshold2: ", threshold2)

    x_test_pred2 = model2.predict(x_test)
    test_mae_loss2 = np.mean(np.abs(x_test_pred2 - x_test), axis=1)
    test_mae_loss2 = test_mae_loss2.reshape((-1))

    anomalies2 = test_mae_loss2 > threshold2

    print(
        "Model 2:",
        accuracy_score(testty, anomalies2),
        precision_score(testty, anomalies2),
        f1_score(testty, anomalies2),
    )
    matrix = confusion_matrix(testty, anomalies2)

    tn, fp, fn, tp = matrix.ravel()

    spec = tn / (tn + fp)

    sens = tp / (tp + fn)

    print("Model2", spec, sens)


if __name__ == "__main__":
    main()
