import json
import logging
import os
import random

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint

from utils.dnn_model import DNNModel
from utils.generator import DataGenerator

random.seed(42)
np.random.seed(42)
tf.random.set_seed(42)

logger = logging.getLogger("tipper")
logger.setLevel(logging.DEBUG)

TOLERANCE = 5
BATCH_SIZE = 100
checkpoint_filepath = "checkpoint/check.h5"
best_weights = "checkpoint/secbest0503.h5"


def main():

    print("Num GPUs Available: ", len(tf.config.list_physical_devices("GPU")))

    with open("utils/config.json") as config_file:
        data = json.load(config_file)

    partition = {"train": [], "validation": [], "test": []}

    for file in os.listdir(data["data_chunked_folder"] + "train"):
        if file.startswith("id-tr"):
            partition["train"].append(str(file.split(".")[0]))
    for file in os.listdir(data["data_chunked_folder"] + "valid"):
        if file.startswith("id-val"):
            partition["validation"].append(str(file.split(".")[0]))
    for file in os.listdir(data["data_chunked_folder"] + "test"):
        if file.startswith("id-te"):
            partition["test"].append(str(file.split(".")[0]))

    train_generator = DataGenerator(
        partition["train"], logger, TOLERANCE, data, "train"
    )
    valid_generator = DataGenerator(
        partition["validation"], logger, TOLERANCE, data, "valid"
    )

    model_checkpoint_callback = ModelCheckpoint(
        filepath=checkpoint_filepath,
        save_weights_only=True,
        monitor="val_accuracy",
        mode="max",
        save_best_only=True,
        verbose=1,
    )

    es = EarlyStopping(monitor="val_accuracy", patience=15, verbose=1, mode="auto")

    DNN = DNNModel()
    DNN.create_model()

    DNN.model.compile(
        optimizer="adam",
        loss=tf.keras.losses.CategoricalCrossentropy(),
        metrics=["accuracy"],
    )

    history = DNN.model.fit(
        x=train_generator,
        batch_size=BATCH_SIZE,
        validation_batch_size=BATCH_SIZE,
        validation_data=valid_generator,
        epochs=50,
        callbacks=[model_checkpoint_callback, es],
    )

    printhist(history)

    print("Evaluation:")
    test_generator = DataGenerator(partition["test"], logger, TOLERANCE, data, "test")
    DNN.model.evaluate(test_generator, verbose=1)

    DNN.model.load_weights(checkpoint_filepath)
    DNN.model.evaluate(test_generator, verbose=1)


def printhist(history):
    plt.plot(history.history["accuracy"])
    plt.plot(history.history["val_accuracy"])
    plt.title("model accuracy")
    plt.ylabel("accuracy")
    plt.xlabel("epoch")
    plt.legend(["train", "validation"], loc="upper left")
    plt.show()

    plt.plot(history.history["loss"])
    plt.plot(history.history["val_loss"])
    plt.title("model loss")
    plt.ylabel("loss")
    plt.xlabel("epoch")
    plt.legend(["train", "validation"], loc="upper left")
    plt.show()


if __name__ == "__main__":
    main()
