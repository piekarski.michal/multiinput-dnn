import json
import logging
import os
import random

import keras_tuner
import numpy as np
import tensorflow as tf
from keras.layers import (
    LSTM, Conv1D, Dense, Dropout, GlobalAveragePooling2D, Input, MaxPooling1D)
from keras.models import Model
from tensorflow.keras.applications import InceptionV3
from utils.generator import DataGenerator

random.seed(42)
np.random.seed(42)
tf.random.set_seed(42)

logger = logging.getLogger("tipper")
logger.setLevel(logging.DEBUG)

TOLERANCE = 5
BATCH_SIZE = 100

IMAGE_SIZE_pin = (134, 390)
NUM_CLASSES = 2
input_shape_pin = (IMAGE_SIZE_pin[0], IMAGE_SIZE_pin[1], 3)
input_shape_vac = (TOLERANCE * 2, 64)


def build_model(hp):
    inc_model = InceptionV3(
        input_shape=input_shape_pin, include_top=False, weights="imagenet"
    )
    inc_model.trainable = False
    layers = inc_model.layers
    layers[-1].trainable = True
    layers[-2].trainable = True
    layers[-3].trainable = True
    if hp.Boolean("unfroze"):
        layers[-4].trainable = True

    pin_inputs = Input(shape=input_shape_pin)
    img = inc_model(pin_inputs, training=False)
    img = GlobalAveragePooling2D()(img)
    img = Dense(500, activation="relu", name="inc_out")(img)
    img_output = Dropout(
        hp.Float("dropout", min_value=0.5, max_value=0.8, sampling="linear")
    )(img)

    cnn_input = Input(shape=input_shape_vac)

    conv = Conv1D(
        filters=hp.Int(f"filtersfirst", min_value=31, max_value=256, step=32),
        kernel_size=hp.Int(f"kernelfirst", min_value=2, max_value=5, step=1),
        padding="same",
        activation="relu",
    )(cnn_input)
    conv = MaxPooling1D(pool_size=2)(conv)

    for i in range(hp.Int("num_layers", 1, 2)):
        conv = Conv1D(
            filters=hp.Int(f"filters_{i}", min_value=64, max_value=256, step=32),
            kernel_size=hp.Int(f"kernel_{i}", min_value=2, max_value=5, step=1),
            padding="same",
            activation="relu",
        )(conv)
        conv = MaxPooling1D(pool_size=2)(conv)
    conv = LSTM(units=hp.Int("unitslstm", min_value=32, max_value=256, step=32))(conv)
    cnn_output = Dense(
        units=hp.Int("unitsdense", min_value=32, max_value=512, step=32),
        activation="relu",
        name="cnn_out",
    )(conv)

    x = tf.keras.layers.concatenate([img_output, cnn_output], name="concat_inv_vgg")

    predictions = Dense(NUM_CLASSES, activation="softmax")(x)
    model = Model(inputs=[pin_inputs, cnn_input], outputs=predictions)
    learning_rate = hp.Float("lr", min_value=1e-4, max_value=1e-2, sampling="log")
    model.compile(
        optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate),
        loss=tf.keras.losses.CategoricalCrossentropy(),
        metrics=["accuracy"],
    )
    return model


def main():
    with open("../utils/config.json") as config_file:
        data = json.load(config_file)

    partition = {"train": [], "validation": []}

    for file in os.listdir(data["data_chunked_folder_tune"] + "train"):
        if file.startswith("id-tr"):
            partition["train"].append(str(file.split(".")[0]))
    for file in os.listdir(data["data_chunked_folder_tune"] + "valid"):
        if file.startswith("id-val"):
            partition["validation"].append(str(file.split(".")[0]))

    train_generator = DataGenerator(
        partition["train"], logger, TOLERANCE, data, "train"
    )
    valid_generator = DataGenerator(
        partition["validation"], logger, TOLERANCE, data, "valid"
    )

    tuner = keras_tuner.RandomSearch(
        hypermodel=build_model,
        objective="val_accuracy",
        max_trials=15,
        executions_per_trial=1,
        overwrite=True,
        directory="tuning",
        project_name="first_tune",
    )
    tuner.search_space_summary()
    tuner.search(train_generator, epochs=10, validation_data=valid_generator)
    models = tuner.get_best_models(num_models=2)
    best_model = models[0]
    best_model.summary()
    tuner.results_summary()


if __name__ == "__main__":
    main()
