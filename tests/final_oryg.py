import tensorflow as tf
from keras.layers import Dense, GlobalAveragePooling2D
from keras.models import Model


IMAGE_SIZE_pin = (340, 134)
NUM_CLASSES = 2
input_shape_pin = (IMAGE_SIZE_pin[0], IMAGE_SIZE_pin[1], 3)

IMAGE_SIZE_vac = (64, 64)
input_shape_vac = (IMAGE_SIZE_vac[0], IMAGE_SIZE_vac[1], 3)


inc_model = tf.keras.applications.inception_v3.InceptionV3(input_shape=input_shape_pin, include_top=False)
inc_output = inc_model.output
img_output = GlobalAveragePooling2D()(inc_output)
img_output = Dense(64, activation= 'softmax', name="inc_out")(img_output)

print(img_output.shape)

cnn_model = tf.keras.applications.vgg16.VGG16(input_shape=input_shape_vac, include_top=False)
cnn_output = cnn_model.output
cnn_output = GlobalAveragePooling2D()(cnn_output)
cnn_output = Dense(64, activation= 'softmax', name="vgg_out")(cnn_output)

print(cnn_output.shape)

x = tf.keras.layers.concatenate([img_output, cnn_output], name="concat_inv_vgg")

predictions = Dense(NUM_CLASSES, activation='linear')(x)
model = Model(inputs = [inc_model.input, cnn_model.input], outputs = predictions)

model.compile(optimizer= "adam", loss=tf.keras.losses.SparseCategoricalCrossentropy(), metrics=['accuracy'])
model.summary()

# tf.keras.utils.plot_model(
#     model,
#     to_file="model_original.png",
#     show_shapes=True,
#     show_dtype=True,
#     show_layer_activations=True,
#     expand_nested=True,
# )
