import json

import h5py
import numpy as np
from tensorflow.keras.applications.inception_v3 import preprocess_input
from tensorflow.keras.utils import Sequence, to_categorical
from sklearn.preprocessing import normalize

with open("../utils/config.json") as config_file:
    data = json.load(config_file)


class MixedDataGenerator(Sequence):
    def __init__(self, batch_size, log, data_pin_name, data_vac_name, tolerance=5):
        self.batch_size = batch_size
        self.data_pin = {}
        self.data_vac = {}
        self.data_len = 0
        self.tolerance = tolerance
        self.vac_times = None
        self.pin_times = None
        self.data_pin_name = data_pin_name
        self.data_vac_name = data_vac_name
        self.log = log
        self.load_data()

    def load_data(self):
        self.data_pin["X"] = []
        self.data_pin["Y"] = []
        self.data_pin["Z"] = []
        self.data_vac["X"] = []
        for f in data[self.data_vac_name]:
            hf = h5py.File(f, "r")
            x_vac = hf.get("data").value
            self.data_vac["X"].extend(x_vac[:, 1:])
            # y_vac = hf.get("meta").value
        self.data_vac["X"] = np.array(self.data_vac["X"])
        for f in data[self.data_pin_name]:
            hf2 = h5py.File(f, "r")
            x_pin = hf2.get("image_data").value
            y_pin = hf2.get("labels").value
            z_pin = hf2.get("scalar_data").value
            self.data_pin["X"].extend(x_pin)
            self.data_pin["Y"].extend(y_pin)
            self.data_pin["Z"].extend(z_pin)
        self.data_pin["X"] = np.array(self.data_pin["X"])
        self.data_pin["Y"] = np.array(self.data_pin["Y"])
        self.data_pin["Z"] = np.array(self.data_pin["Z"])
        self.data_len = self.data_pin["X"].shape[0]
        self.vac_times = self.data_vac["X"][:, 0]
        self.pin_times = self.data_pin["Z"][:, 0]

    def __len__(self):
        """Denotes the number of batches per epoch
        :return: number of batches per epoch
        """
        return int(self.data_len / self.batch_size)

    def __getitem__(self, index):
        """Generate one batch of data
        :param index: index of the batch
        :return: X and y when fitting. X only when predicting
        """
        X_pin = self.data_pin["X"][
            index * self.batch_size : index * self.batch_size + self.batch_size
        ]
        X_pin_st = np.array(
            [
                np.stack(
                    (preprocess_input(el), preprocess_input(el), preprocess_input(el)),
                    axis=-1,
                )
                for el in X_pin
            ]
        )
        Y_pin = to_categorical(
            self.data_pin["Y"][
                index * self.batch_size : index * self.batch_size + self.batch_size
            ],
            num_classes=2,
            dtype=bool,
        )
        X_vac = self._generateVac(index)
        return [X_pin_st, X_vac], Y_pin

    def _generateVac(self, index):
        """
        Generate sliding window over vac signals
        :param index: index of the batch
        :return: X of sliding windows
        """
        X = []
        for i in range(self.batch_size):
            current_index = index * self.batch_size + i
            vac_mid_ind = np.argmin(abs(self.vac_times - self.pin_times[current_index]))
            if (vac_mid_ind - self.tolerance) < 0:
                # beginning
                X.append(
                    normalize(self.data_vac["X"][vac_mid_ind : vac_mid_ind + 2 * self.tolerance], axis=0)
                )
            elif (vac_mid_ind + self.tolerance) > self.data_len:
                # end
                X.append(
                    normalize(self.data_vac["X"][vac_mid_ind - 2 * self.tolerance : vac_mid_ind], axis=0)
                )
            else:
                X.append(
                    normalize(self.data_vac["X"][
                        vac_mid_ind - self.tolerance : vac_mid_ind + self.tolerance
                    ], axis=0)
                )
        return np.array(X)
